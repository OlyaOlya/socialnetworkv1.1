from django.contrib import admin
from network.models import User, Posts, Comments, UserProfile


class UserProfileAdmin(admin.ModelAdmin):
    models = UserProfile
    list_display = ('id', 'full_name', 'date_of_birth', 'about_me')

class PostsAdmin(admin.ModelAdmin):
    models = Posts
    list_display = ('id', 'post_text', 'pub_date', 'user')

class CommentsAdmin(admin.ModelAdmin):
    models = Comments
    list_display = ('id', 'comment_text', 'post', 'date')


admin.site.register(Posts, PostsAdmin)
admin.site.register(Comments, CommentsAdmin)
admin.site.register(UserProfile, UserProfileAdmin)
